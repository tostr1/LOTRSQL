const sql = require('mssql');
const http = require('http');

connection = new sql.ConnectionPool({
    user: 'sql_stroinski',
    password: '',
    server: '172.22.0.69',
    database: 'il_devguest'
});
 
connection.connect();

module.exports.getAllRegions = function(req, res) {
    const request = new sql.Request(connection);
    request.query('SELECT * FROM LOTR_Region', (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })    
}

module.exports.getRegionsByName = function(req, res, name) {
    console.log(name);
    const request = new sql.Request(connection);
    SQLString = "SELECT * FROM LOTR_Region WHERE RegionName LIKE '%" + name + "%'";
    request.query(SQLString, (err, result) => {
        if(err) {
            throw(err);
            console.log(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })
}

module.exports.getRegionByID = function(req, res, id) {
    const request = new sql.Request(connection);
    request.query(`SELECT * FROM LOTR_Region WHERE Region_ID=${id}`, (err, result) => {
        if(err) {
            throw(err);
            console.log(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })
}

module.exports.getAllLanguages = function(req, res) {
    const request = new sql.Request(connection);
    request.query('SELECT * FROM LOTR_Language', (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })    
}

module.exports.getLanguagesByName = function(req, res, name) {
    const request = new sql.Request(connection);
    SQLString = "SELECT * FROM LOTR_Language WHERE LanguageName LIKE '%" + name + "%'";
    request.query(SQLString, (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })    
}

module.exports.getLanguageByID = function(req, res, id) {
    const request = new sql.Request(connection);
    request.query(`SELECT * FROM LOTR_Language WHERE Language_ID=${id}`, (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })
}

module.exports.getHumanLanguages = function(req, res) {
    const request = new sql.Request(connection);
    request.query('SELECT * FROM LOTR_Language WHERE FlagHuman=1', (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })
}

module.exports.getElvishLanguages = function(req, res) {
    const request = new sql.Request(connection);
    request.query('SELECT * FROM LOTR_Language WHERE FlagElvish=1', (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })
}

module.exports.getDwarvishLanguages = function(req, res) {
    const request = new sql.Request(connection);
    request.query('SELECT * FROM LOTR_Language WHERE FlagDwarvish=1', (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })
}

module.exports.getAllSights = function(req, res) {
    const request = new sql.Request(connection);
    request.query('SELECT * FROM LOTR_Sights', (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })    
}

module.exports.getSightsByName = function(req, res, name) {
    const request = new sql.Request(connection);
    SQLString = "SELECT * FROM LOTR_Sights WHERE SightName LIKE '%" + name + "%'";
    request.query(SQLString, (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })    
}

module.exports.getSightByID = function(req, res, id) {
    const request = new sql.Request(connection);
    request.query(`SELECT * FROM LOTR_Sights WHERE Sight_ID=${id}`, (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })
}

module.exports.getSightByRegionID = function(req, res, id) {
    const request = new sql.Request(connection);
    request.query(`SELECT r.RegionName, s.* FROM LOTR_Sights s LEFT JOIN LOTR_Region r ON r.Region_ID = s.Region_ID WHERE s.Region_ID=${id}`, (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })
}

module.exports.getSightsByRegionName = function(req, res, name) {
    const request = new sql.Request(connection);
    SQLString = "SELECT s.Sight_ID, r.RegionName, s.SightName, s.Region_ID, s.DateCreated, s.DateLastModified FROM LOTR_Region r LEFT JOIN LOTR_Sights s ON s.Region_ID = r.Region_ID WHERE RegionName LIKE '%" + name + "%'";
    request.query(SQLString, (err, result) => {
        if(err) {
            throw(err);
        }
        res.writeHead(200, {'Content-Type': 'text/json'});
        res.write(JSON.stringify(result.recordset));
        res.end();
    })    
}