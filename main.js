const http = require('http');
const port = 3000;
const fs = require('fs');
const sql = require('./sql.js');
const url = require('url');
const isNumber = require('is-number');

const requestHandler = (request, response) => {
  console.log(request.url);
  const queryStrings = url.parse(request.url, true).query;
  if(request.url == '/') {
    fs.readFile('./index.html', function(err, html) {
        if (err) {
            throw(err);
        }
        response.writeHeader(200, {"Content-Type": "text/html"});  
        response.write(html);  
        response.end();  
    })    
  }

  // REGIONS
  if(request.url == '/RegionsAll') {
    sql.getAllRegions(request, response);
  }

  if(request.url.includes('/RegionsName')) {
    if(queryStrings.parameter != '') {
      sql.getRegionsByName(request, response, queryStrings.parameter);
    } else {
      response.write("No Data");
    }
  }

  if(request.url.includes('/RegionsID')) {
    if(queryStrings.parameter != '' && isNumber(queryStrings.parameter)) {
      sql.getRegionByID(request, response, queryStrings.parameter);
    } else {
      response.write("No Data");
    }
  }

  // LANGUAGES
  if(request.url == '/LanguagesAll') {
    sql.getAllLanguages(request, response);
  }

  if(request.url.includes('/LanguagesName')) {
    if(queryStrings.parameter != '') {
      sql.getLanguagesByName(request, response, queryStrings.parameter);
    } else {
      response.write("No Data");
    }
  }

  if(request.url.includes('/LanguagesID')) {
    if(queryStrings.parameter != '' && isNumber(queryStrings.parameter)) {
      sql.getLanguageByID(request, response, queryStrings.parameter);
    } else {
      response.write("No Data");
    }
  }

  if(request.url == '/LanguagesHuman') {
    sql.getHumanLanguages(request, response);
  }

  if(request.url == '/LanguagesElvish') {
    sql.getElvishLanguages(request, response);
  }

  if(request.url == '/LanguagesDwarvish') {
    sql.getDwarvishLanguages(request, response);
  }

  // SIGHTS
  if(request.url == '/SightsAll') {
    sql.getAllSights(request, response);
  }

  if(request.url.includes('/SightsName')) {
    if(queryStrings.parameter != '') {
      sql.getSightsByName(request, response, queryStrings.parameter);
    } else {
      response.write("No Data");
    }
  }

  if(request.url.includes('/SightsID')) {
    if(queryStrings.parameter != '' && isNumber(queryStrings.parameter)) {
      sql.getSightByID(request, response, queryStrings.parameter);
    } else {
      response.write("No Data");
    }
  }

  if(request.url.includes('/SightsRegionID')) {
    if(queryStrings.parameter != '' && isNumber(queryStrings.parameter)) {
      sql.getSightByRegionID(request, response, queryStrings.parameter);
    } else {
      response.write("No Data");
    }
  }

  if(request.url.includes('/SightsRegionName')) {
    if(queryStrings.parameter != '') {
      sql.getSightsByRegionName(request, response, queryStrings.parameter);
    } else {
      response.write("No Data");
    }
  }
}

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log('server is listening on ' + port)
})