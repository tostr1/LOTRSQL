# LOTR SQL Projekt
  
## Aufgabenstellung
- Erstellung einer Datenbank, welche Inhalte der Fantasytriologie "Herr der Ringe" kategorisch gliedert
- freie Konzeptionalisierung
  
## Ablauf im Detail
  - #### Auswahl einer Datenbanksprache
    Da von Anfang an klar war die Daten mit einem relationalen Datenbanksystem zu speichern, grenzte sich die Auswahl
    auf MySQL von Oracle oder SQL Server von Microsoft ein.  
    Das erste Konzept wurde in SQL Server erstellt. Hier war der Faktor der vorherigen Erfahrung entscheidend. Durch Kompatibilitätsprobleme in Zusammenhang mit der Nutzung unter Linux, fiel die Wahl auf MySQL.
  - #### Welche Daten sollen gespeichert werden?
    Durch die offen gestaltete Aufgabenstellung konnten wir das Konzept und den Zweck der Datenbank frei wählen.
    Wir haben uns hier entschieden eine Datenbank mit den Regionen von Mittel-Erde und einigen der wichtigen Orten zu erstellen. Ebenfalls sollen die Sprachen der einzelnen Regionen oder Orte erfasst werden, und von welchen Rassen diese Sprachen gesprochen werden.  
      
    Die Datenbank dient der Veranschaulichung und stellt nicht eine komplette Liste von Orten und Regionen dar.

  - #### Erstellung eines Datenbankmodells
    Da die Daten es anbieten, nutzen wir die relationale Natur von MySQL aus.  
    Wir haben für den Großteil der Daten drei Tabellen erstellt.  
    * #### LOTR_Region
        *  Diese Tabelle beinhaltet die übergreifenden Regionen von Mittel-Erde 
        *  Aktuell wird hier lediglich der Regionsname erfasst
    * #### LOTR_Language
        * Hier werden die verschiedenen gesprochenen Sprachen von Mittel-Erde erfasst
        * Ebenfalls wird zwischen menschlichen, elfischen und Zwergensprachen unterschieden
    * #### LOTR_Sights
        * Diese Tabelle enthält die verschiedenen Orte, Sehenswürdigkeiten und Städte von Mittel-Erde
        * Neben dem Namen des Ortes wird hier ebenfalls mit der zugehörigen Region verknüpft    
    * #### Relationstabellen
        * Um, zum Beispiel, alle Sprachen die in einer Region gesprochen werden anzuzeigen haben wir uns entschieden noch weitere sogenannte Relationstabellen zu erstellen
        * Diese enthalten nichts weiter als ihre ID und zwei oder mehr Fremdschlüssel
        * Das ermöglicht uns das vorher genannte Szenario ohne eine komplizierte SQL Abfrage durchzuführen

  - #### Erstellung eines Datenbankscripts  

    Damit unsere Datenbank flexibel bleibt, haben wir ein sogenanntes Kick-Off Script erstellt.  
    Der Aufbau ermöglicht uns das Script ohne weitere Konfiguration oder Parameter auszuführen und die Datenbank binnen Sekunden an anderer Stelle verwenden zu können.  
    
    Zum Anfang des Scriptes beseitigen wir jegliche Tabellen, Fremdschlüssel und Primärschlüssel, sofern vorhanden. Daraufhin werden die Tabellen und zugehörige Schlüssel wieder erstellt, und dann mit Daten befüllt.
    
    Das ermöglicht uns ebenfalls die Datenbank zu jedem Zeitpunkt auf den Ausgangszustand zurückzusetzen sollten Fehler auftreten, etc.
    
  - #### Recherche
    
    Der primäre Anteil der Daten wurde aus dem [offiziellen Herr der Ringe Wiki](http://lotr.wikia.com/wiki/) bezogen.  
    Abschließend wurden die Daten mit anderen Quellen und eigenem Wissen verglichen.