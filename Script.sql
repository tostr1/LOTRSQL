IF OBJECT_ID('dbo.FK_Region_ID_LOTR_Sights', 'F') IS NOT NULL
BEGIN
	ALTER TABLE LOTR_Sights
	DROP CONSTRAINT FK_Region_ID_LOTR_Sights
END
GO

IF OBJECT_ID('dbo.FK_Language_ID_LOTR_Language_Region_REL', 'F') IS NOT NULL
BEGIN
	ALTER TABLE LOTR_Language_Region_REL
	DROP CONSTRAINT FK_Language_ID_LOTR_Language_Region_REL
END
GO

IF OBJECT_ID('dbo.FK_Region_ID_LOTR_Language_Region_REL', 'F') IS NOT NULL
BEGIN
	ALTER TABLE LOTR_Language_Region_REL
	DROP CONSTRAINT FK_Region_ID_LOTR_Language_Region_REL
END
GO

IF EXISTS (SELECT NAME FROM sysobjects WHERE xtype='u' AND name = 'LOTR_Region')
BEGIN
	DROP TABLE LOTR_Region
END
GO

IF EXISTS (SELECT NAME FROM sysobjects WHERE xtype='u' AND name = 'LOTR_Sights')
BEGIN
	DROP TABLE LOTR_Sights
END
GO

IF EXISTS (SELECT NAME FROM sysobjects WHERE xtype='u' AND name = 'LOTR_Language')
BEGIN
	DROP TABLE LOTR_Language
END
GO

IF EXISTS (SELECT NAME FROM sysobjects WHERE xtype='u' AND name = 'LOTR_Language_Region_REL')
BEGIN
	DROP TABLE LOTR_Language_Region_REL
END
GO

CREATE TABLE LOTR_Region
(
	Region_ID [int] IDENTITY(1,1) NOT NULL,
	RegionName [varchar] (150) NOT NULL,
	Status_ID [int] DEFAULT 1,
	DateCreated [DATETIME] DEFAULT GETDATE() NULL,
	DateLastModified [DATETIME] DEFAULT GETDATE() NULL,	
	CONSTRAINT PK_LOTR_Region_Region_ID PRIMARY KEY CLUSTERED (Region_ID)
)
GO

INSERT INTO LOTR_Region (RegionName) VALUES ('Gondor')
INSERT INTO LOTR_Region (RegionName) VALUES ('Rohan')
INSERT INTO LOTR_Region (RegionName) VALUES ('Eriador')
INSERT INTO LOTR_Region (RegionName) VALUES ('The Shire')
INSERT INTO LOTR_Region (RegionName) VALUES ('Misty Mountains')
INSERT INTO LOTR_Region (RegionName) VALUES ('Rhovanion')
INSERT INTO LOTR_Region (RegionName) VALUES ('Rhûn')
INSERT INTO LOTR_Region (RegionName) VALUES ('Mordor')
GO

CREATE TABLE LOTR_Sights
(
	Sight_ID [int] IDENTITY(1,1) NOT NULL,
	SightName [varchar] (150) NOT NULL,
	Region_ID [int] NOT NULL,
	Status_ID [int] DEFAULT 1 NULL,
	DateCreated [DATETIME] DEFAULT GETDATE() NULL,
	DateLastModified [DATETIME] DEFAULT GETDATE() NULL,
	CONSTRAINT PK_LOTR_Sights_Sight_ID PRIMARY KEY CLUSTERED (Sight_ID)
)
GO

INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Hobbiton', 4)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Bree', 4)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Barrow Downs', 4)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Old Forest', 4)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Weathertop', 4)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Rivendell', 5)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Moria', 5)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Lorien', 6)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Fangorn', 2)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Isengard', 5)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Mirkwood', 6)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Emyn Muil', 2)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Dead Marshes', 8)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Osgiliath', 1)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Minas Tirith', 1)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Minas Morgul', 8)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Sea of Núrnen', 8)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Mount Doom (Orodruin)', 8)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Barad-Dúr', 8)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Helms Deep', 2)
INSERT INTO LOTR_Sights (SightName, Region_ID) VALUES ('Edoras', 2)

CREATE TABLE LOTR_Language
(
	Language_ID [INT] IDENTITY(1,1) NOT NULL,
	LanguageName [VARCHAR] (75) NOT NULL,
	FlagHuman [SMALLINT] NOT NULL,
	FlagElvish [SMALLINT] NOT NULL,
	FlagDwarvish [SMALLINT] NOT NULL,
	Status_ID [int] DEFAULT 1 NULL,
	DateCreated [DATETIME] DEFAULT GETDATE() NULL,
	DateLastModified [DATETIME] DEFAULT GETDATE() NULL,	
	CONSTRAINT PK_LOTR_Language_Language_ID PRIMARY KEY CLUSTERED (Language_ID)
)
GO 

INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Westron', 1, 0, 0)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Hobbitish', 1, 0, 0)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Khuzdûl', 0, 0, 1)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Entish', 1, 0, 0)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Valarin', 1, 0, 0)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Quenya', 0, 1, 0)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Telerin', 0, 1, 0)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Sindarin', 0, 1, 0)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Black Speech', 0, 0, 0)
INSERT INTO LOTR_Language (LanguageName, FlagHuman, FlagElvish, FlagDwarvish) VALUES ('Rohirric', 1, 0, 0)
GO

CREATE TABLE LOTR_Language_Region_REL
(
	Language_Region_REL_ID [INT] IDENTITY(1,1) NOT NULL,
	Language_ID [INT] NOT NULL,
	Region_ID [INT] NOT NULL,
	Status_ID [int] DEFAULT 1 NULL,
	DateCreated [DATETIME] DEFAULT GETDATE() NULL,
	DateLastModified [DATETIME] DEFAULT GETDATE() NULL,	
	CONSTRAINT PK_LOTR_Language_Region_REL_Language_Region_REL_ID PRIMARY KEY CLUSTERED (Language_Region_REL_ID)
)
GO

ALTER TABLE LOTR_Sights
ADD CONSTRAINT FK_Region_ID_LOTR_Sights
FOREIGN KEY (Region_ID)
REFERENCES LOTR_Region(Region_ID)

ALTER TABLE LOTR_Language_Region_REL
ADD CONSTRAINT FK_Language_ID_LOTR_Language_Region_REL
FOREIGN KEY (Language_ID)
REFERENCES LOTR_Language(Language_ID)

ALTER TABLE LOTR_Language_Region_REL
ADD CONSTRAINT FK_Region_ID_LOTR_Language_Region_REL
FOREIGN KEY (Region_ID)
REFERENCES LOTR_Region(Region_ID)

INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 1)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 2)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 3)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 4)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 5)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 6)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 7)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 8)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (2, 4)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (10, 2)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (10, 7)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (1, 7)
INSERT INTO LOTR_Language_Region_REL (Language_ID, Region_ID) VALUES (8, 6)